buf = new Buffer(256);
len = buf.write("Simply Easy Learning...");

console.log("Octets written : "+  len);

console.log(buf.toString("utf8",9,12));

console.log(buf.toJSON());

console.log(new Buffer([1,2,3,4,49]).toString("ascii"));

console.log("---------------------------------");
console.log(Buffer.concat([new Buffer([1,2,3,4,49]), buf], 500).toString());

console.log("---------------------------------");
var buffer1 = new Buffer('ABC');
//copy a buffer
var buffer2 = new Buffer(3);
buffer1.copy(buffer2);
console.log("buffer2 content: " + buffer2.toString());

