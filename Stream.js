var fs = require("fs");
var data = '';
var i = 0;

// Create a readable stream
var readerStream = fs.createReadStream('Buffer.js');

// Set the encoding to be utf8. 
readerStream.setEncoding('UTF8');

// Handle stream events --> data, end, and error
readerStream.on('data', function(chunk) {
   data += chunk;
   i++;
});

readerStream.on('end',function(){
   console.log(data);
   console.log("Total Chunks: "+i);
});

readerStream.on('error', function(err){
   console.log(err.stack);
});

console.log("Program Ended");